################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../build/AddressSpace/src/ASInformationModel.cpp \
../build/AddressSpace/src/AddressSpaceClasses.cpp \
../build/AddressSpace/src/SourceVariables.cpp 

OBJS += \
./build/AddressSpace/src/ASInformationModel.o \
./build/AddressSpace/src/AddressSpaceClasses.o \
./build/AddressSpace/src/SourceVariables.o 

CPP_DEPS += \
./build/AddressSpace/src/ASInformationModel.d \
./build/AddressSpace/src/AddressSpaceClasses.d \
./build/AddressSpace/src/SourceVariables.d 


# Each subdirectory must supply rules for building sources it contributes
build/AddressSpace/src/%.o: ../build/AddressSpace/src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



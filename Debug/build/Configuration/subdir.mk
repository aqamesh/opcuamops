################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CXX_SRCS += \
../build/Configuration/Configuration.cxx 

CPP_SRCS += \
../build/Configuration/ConfigValidator.cpp \
../build/Configuration/Configurator.cpp 

CXX_DEPS += \
./build/Configuration/Configuration.d 

OBJS += \
./build/Configuration/ConfigValidator.o \
./build/Configuration/Configuration.o \
./build/Configuration/Configurator.o 

CPP_DEPS += \
./build/Configuration/ConfigValidator.d \
./build/Configuration/Configurator.d 


# Each subdirectory must supply rules for building sources it contributes
build/Configuration/%.o: ../build/Configuration/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

build/Configuration/%.o: ../build/Configuration/%.cxx
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



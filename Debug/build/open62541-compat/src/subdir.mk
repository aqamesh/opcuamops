################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../build/open62541-compat/src/nodemanagerbase.cpp \
../build/open62541-compat/src/opcua_basedatavariabletype.cpp \
../build/open62541-compat/src/open62541_compat.cpp \
../build/open62541-compat/src/statuscode.cpp \
../build/open62541-compat/src/uabytearray.cpp \
../build/open62541-compat/src/uabytestring.cpp \
../build/open62541-compat/src/uadatavalue.cpp \
../build/open62541-compat/src/uadatavariablecache.cpp \
../build/open62541-compat/src/uadatetime.cpp \
../build/open62541-compat/src/uanodeid.cpp \
../build/open62541-compat/src/uaserver.cpp \
../build/open62541-compat/src/uastring.cpp \
../build/open62541-compat/src/uavariant.cpp 

OBJS += \
./build/open62541-compat/src/nodemanagerbase.o \
./build/open62541-compat/src/opcua_basedatavariabletype.o \
./build/open62541-compat/src/open62541_compat.o \
./build/open62541-compat/src/statuscode.o \
./build/open62541-compat/src/uabytearray.o \
./build/open62541-compat/src/uabytestring.o \
./build/open62541-compat/src/uadatavalue.o \
./build/open62541-compat/src/uadatavariablecache.o \
./build/open62541-compat/src/uadatetime.o \
./build/open62541-compat/src/uanodeid.o \
./build/open62541-compat/src/uaserver.o \
./build/open62541-compat/src/uastring.o \
./build/open62541-compat/src/uavariant.o 

CPP_DEPS += \
./build/open62541-compat/src/nodemanagerbase.d \
./build/open62541-compat/src/opcua_basedatavariabletype.d \
./build/open62541-compat/src/open62541_compat.d \
./build/open62541-compat/src/statuscode.d \
./build/open62541-compat/src/uabytearray.d \
./build/open62541-compat/src/uabytestring.d \
./build/open62541-compat/src/uadatavalue.d \
./build/open62541-compat/src/uadatavariablecache.d \
./build/open62541-compat/src/uadatetime.d \
./build/open62541-compat/src/uanodeid.d \
./build/open62541-compat/src/uaserver.d \
./build/open62541-compat/src/uastring.d \
./build/open62541-compat/src/uavariant.d 


# Each subdirectory must supply rules for building sources it contributes
build/open62541-compat/src/%.o: ../build/open62541-compat/src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



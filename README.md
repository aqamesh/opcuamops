# Opcua-Mops
Opcua-mops server is project created from quasar framework. This means that all the libraries needed for the server needs to be built from the quasar project.<br/>
Quasar originated as an internal project at CERN, but now is used also by commercial companies and for private, open-source projects [For more information visit [Quasar homepage](https://github.com/quasar-team/quasar)]
```
For more information contact ahmed.qamesh@cern.ch
```
# Dependancies
Once you've cloned the Opcua-mops git repository to your development machine you'll need to obtain some dependencies before you can start developing. 
Due to licensing, these dependencies can't be distributed with the quasar code, but please read [Installing Quasar dependancies](https://gitlab.cern.ch/aqamesh/opcuamops/-/wikis/Installing-Quasar-dependancies) page.

# Documentation
Documentation can be found under: https://gitlab.cern.ch/aqamesh/opcuamops/-/wikis/home

# Building the project  and usage
1. Clone the repository to get a copy of the source code (for developers):<pre><code>git clone ssh://git@gitlab.cern.ch:7999/aqamesh/opcuamops.git</code></pre>
2. Clean any old building directories<pre><code>rm -rf build</code></pre>
3. Build the server:<pre><code>./quasar.py build</code></pre>
4. Run the quasar OPCUA server:<pre><code>cd ~/tmp/opcua-server/build/bin
./OpcUaServer</code></pre>